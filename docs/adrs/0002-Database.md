# Usar MongoDB
2018-01-19

## Status
Accepted

## Context
La información extraida sobre los personajes de Naruto debe ser almacenada para construir un API sobre ella.

Dado que el detalle de los personajes tiene informacion variada, no estructurada, de tipo documento, se decide usar mongodb.

## Decision
Usar mongodb

