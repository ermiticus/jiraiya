# Usar Axios y CheerioJS para hacer el scrapping
2018-01-18

## Status
Accepted

## Context
Existe la necesidad de extraer los caracteres de naruto y su detalle de la pagina del proveedor https://naruto.fandom.com/wiki/Category:Characters. Para luego exponerlos en un API hecho en nodejs

El script debe estar disponible para correr de manera automatizada.

Se evaluo entre hacer el scrapping con python y las librerias requests y scrappy o nodejs y las librerias axios y cheeriojs.

Dado que el API debe estar en nodejs se decide hacer el script con node axios y cheerios

## Decision
Usar nodejs axio y cheeriosjs 

## Consequences
Pros: Se mantiene un solo lenguaje de programacion para el equipo.
Cons: Ninguno evidente al mom