'use strict'

const parse = require('./naruto/parse')
const parseDetail = require('./naruto/parseDetail')
const database = require('./helpers/database')
require('./api/models/Character')

async function extract() {
  let nextUrl = 'https://naruto.fandom.com/wiki/Category:Characters'
  let charactersUrls = []

  while (typeof nextUrl !== 'undefined') {
    try {
      let result = await parse(nextUrl)

      nextUrl = result.nextUrl[0]
      charactersUrls = result.charactersUrls
    } catch (err) {
      console.log(err.message)
    }

    const coll = db.connection.collections.characters
    const bulk = coll.initializeUnorderedBulkOp()
    const charArrPromises = Array.prototype.map.call(
      charactersUrls,
      async characterUrl => {
        try {
          let detail = await parseDetail(characterUrl)
          return detail
        } catch (err) {
          if (err.code === 'ETIMEDOUT') {
            console.error(`ERROR => ETIMEDOUT: Requesting ${characterUrl}`)
          }
        }
      }
    )

    const details = await Promise.all(charArrPromises)
    Array.prototype.map.call(details, detail => {
      bulk
        .find({ name: detail.name })
        .upsert()
        .update({$set: detail})
    })
    bulk
      .execute()
      .then(result => console.log(result))
      .catch(err => console.log(err))
  }
}

const db = new database()
db.connection.on('connected', err => {
  extract()
    .then(() => db.disconnect())
    .catch(err => console.log(err))
})
