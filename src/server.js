'use strict'

const express = require('express')
const cors = require('cors')
const bodyParser = require('body-parser')

const logger = require('./helpers/logger')
const database = require('./helpers/database')
const charactersRoutes = require('./api/routes/characters')
const usersRoutes = require('./api/routes/users')
const authMiddleware = require('./api/middleware/auth')
const errorMiddleware = require('./api/middleware/error')

const app = express()

app.use(bodyParser.urlencoded({extended: true}))
app.use(bodyParser.json())

// static files
app.use(express.static('dist'))

app.use(cors())
app.use(authMiddleware())

// routes
app.use('/characters', charactersRoutes)
app.use('/users', usersRoutes)

// error capture
app.use(errorMiddleware)


const db = new database()
db.connection.on('open', err => {
  app.listen(80, () => logger.captureMessage('API server listening'))
})

// required export for testing support
module.exports = app
