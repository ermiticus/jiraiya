'use strict'

const express = require('express')
const router = express.Router()

const logger = require('../../helpers/logger')
const charactersController = require('../controller/characters')

async function getIndex(req, res, next) {
    try {
        const result = await charactersController.getIndex()
        res.json(result)
    } catch (error) {
        logger.captureException(error)
    }
}

async function getById(req, res, next) {
    try {
        const result = await charactersController.findById(req.params.id)
        res.json(result)
    } catch (error) {
        logger.captureException(error)
    }
}

async function getIndexOnLetter(req, res, next) {
    try {
        const result = await charactersController.findByInitial(req.params.letter)
        res.json(result)
    } catch (error) {
        logger.captureException(error)
    }
}

router.get('/', getIndex)

router.get('/:id', getById)

router.get('/indexon/:letter', getIndexOnLetter)

module.exports = router
