'use strict'

const express = require('express')
const router = express.Router()

const usersController = require('../controller/user')

async function exists(req, res, next) {
    try {
        const exists = await usersController.exists(req.body)
        res.status(200).json(exists)
    } catch (error) {
        next(error)
    }
}

async function signup(req, res, next) {
    try {
        const exists = await usersController.exists(req.body)
        if (exists) {
            res.status(200).json(!exists)
            return
        }
        const successfulSignup = await usersController.signup(req.body)
        if (successfulSignup) {
            res.status(201).json(successfulSignup)
        }        
    } catch (error) {
        next(error)
    }
}

async function login(req, res, next) {
    try {
        const successfulLogin = await usersController.login(req.body)
        if(successfulLogin) {
            res.status(200).json(successfulLogin)
        }
        else {
            res.status(400).json(successfulLogin)
        }        
    } catch (error) {
        next(error)
    }
}

router.post('/exists', exists)

router.post('/login', login)

router.post('/signup', signup)

module.exports = router
