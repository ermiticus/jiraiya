'use strict'

const mongoose = require('mongoose')
const Schema = mongoose.Schema

const usersSchema = new Schema({
  email: String,
  password: String,
  updated: { type: Date, default: Date.now }
})

const User = mongoose.model('User', usersSchema)

module.exports = User
