'use strict'

const mongoose = require('mongoose')
const Schema = mongoose.Schema

const charactersSchema = new Schema({
  name: String,
  imgSrc: String,
  info: String,
  updated: { type: Date, default: Date.now }
})

const Character = mongoose.model('Character', charactersSchema)

module.exports = Character
