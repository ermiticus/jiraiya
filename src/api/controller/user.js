'use strict';

const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const config = require('../../config/config')


const User = require('../models/User');

const saltRound = 12

async function findUser(email) {
  try {
    const q = await User.find({ email: email }, { email: 1, password: 1 }).exec();
    
    return q
  } catch (error) {
    throw error
  }
}

async function exists({email}) {
  try {
    const q = await findUser(email)
    
    return q.length > 0
  } catch (err) {
    throw err;
  }
}

async function signup({email, password}) {
  try {
    const hashed = await bcrypt.hash(password, saltRound)

    const user = new User({
      email: email,
      password: hashed
    })

    const saved = user.save()
    return typeof saved === 'object'
  } catch (error) {
    throw error
  }
}

async function login({email, password}) {
  try {
    let userMatch = await findUser(email)
    
    if(userMatch.length > 0) {
      userMatch = userMatch[0]
      const validPassword = await bcrypt.compare(password, userMatch.password)
      if(validPassword) {
        const token = jwt.sign({ sub: userMatch._id }, config.authSecret);
        return {
          email: userMatch.email,
          token
        }
      }
    }
    return false 
  } catch (error) {
    throw error
  }
}

module.exports = {
  exists,
  signup,
  login
};
