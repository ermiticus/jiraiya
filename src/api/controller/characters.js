'use strict'

const Character = require('../models/Character')

async function getIndex() {
  try {
    const q = await Character.aggregate([
      {
        $group: {
          _id: {
            letter: {
              $substrCP: ['$name', 0, 1]
            }
          }
        }
      }
    ]).exec()

    const letters = Array.prototype.map.call(q, item => {
      return item._id.letter
    })

    return letters.sort()
  } catch (err) {
    throw err
  }
}

async function findById(id) {
  try {
    const q = await Character.findById(id).exec()
    return q
  } catch (err) {
    throw err
  }
}

async function findByInitial(letter) {
  try {
    const q = await Character.find(
      { name: { $regex: '^' + letter, $options: 'i' } },
      { name: 1 }
    ).exec()
    return q
  } catch (err) {
    throw err
  }
}

module.exports = {
  getIndex,
  findByInitial,
  findById
}
