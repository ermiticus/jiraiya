'use strict'

const logger = require('../../helpers/logger')

function error(err, req, res, next) {
  logger.captureException(err)

  if (typeof err === 'string') {
    return res.status(400).json({ message: err })
  }
  if (err.name === 'UnauthorizedError') {
    return res
      .status(401)
      .json({ message: 'Unauthorized, invalid credentials' })
  }

  return res.status(500).json({ message: err.message })
}

module.exports = error
