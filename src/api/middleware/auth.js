'use strict'

const jwt = require('express-jwt')
const config = require('../../config/config')

function jwtAuth() {
  const secret = config.authSecret
  return jwt({ secret }).unless({
    path: config.publicPaths
  })
}

module.exports = jwtAuth
