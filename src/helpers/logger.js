'use strict'

const Sentry = require('@sentry/node')
Sentry.init({
  dsn: 'https://f64f4c85c0794eb9ac1c0208ff5cea55@sentry.io/1375544'
})

module.exports = Sentry