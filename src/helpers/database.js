'use strict'

const mongoose = require('mongoose')
const config = require('../config/config')

function database() {
  // init db connection and returns
  mongoose.connect(
    config.connString,
    config.dbOptions
  )
  return mongoose
}

module.exports = database
