const store = {
  apiBaseUrl: '',
  state: {
    letter: '',
    id: '',
    token: null
  },
  setLetter(letter) {
    this.state.letter = letter
    this.state.id = ''
    
  },
  setId(id){
    this.state.id = id
  },
  setToken(token) {
    this.state.token = token
  }
}

module.exports = {
  store: store
}
