import Vue from 'vue'

import App from './App.vue'
import router from './router'
import { store } from './store'
import config from '../config/config'

const url =
  process.env.NODE_ENV === 'production'
    ? config.apiBaseUrl
    : config.apiBaseUrlDev
store.apiBaseUrl = url

export default new Vue({
  el: '#app',
  router,
  render: r => r(App)
})
