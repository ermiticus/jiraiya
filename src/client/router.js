import Vue from 'vue'
import VueRouter from 'vue-router'

import config from '../config/config'
import { store } from './store'
import Login from './components/Login.vue'
import SignUp from './components/SignUp.vue'
import AppLayout from './components/AppLayout.vue'

Vue.use(VueRouter)

const router = new VueRouter({
   mode:'history',
   routes:[
      {path:'/', name:'app-layout', component:AppLayout},
      {path: '/login', name:'login', component: Login},
      {path: '/sign-up', name:'sign-up', component: SignUp},
      {path: '*', redirect: '/'}
   ] 
})

router.beforeEach((to, from, next) => {
    const requiresAuth = !config.publicPages.includes(to.path)
    const loggedIn = store.state.token !== null

    if(requiresAuth && !loggedIn) {
      return next({
        path: '/login',
        params: { nextUrl: to.fullPath }
      })
    }

    next()
  })

export default router