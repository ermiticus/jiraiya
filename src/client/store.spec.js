'use strict'

const { store } = require('./store')
const expect = require('chai').expect

describe('Store module', () => {
  describe('"store"', () => {
    it('Should export an object', () => {
      expect(store).to.be.an('object')
    })

    it('Should expose state.letter property', () => {
      expect(store.state).to.have.property('letter')
    })

    it('Should expose setLetter method', () => {
      expect(store.setLetter).to.be.a('function')
    })

    it('setLetter method should modify letter state', () => {
      store.setLetter('B')
      expect(store.state.letter).to.equal('B')
    })

    it('Should expose state.id property', () => {
      expect(store.state).to.have.property('id')
    })

    it('Should expose setId method', () => {
      expect(store.setId).to.be.a('function')
    })

    it('setId method should modify id state', () => {
      store.setId('David')
      expect(store.state.id).to.equal('David')
    })

    it('Should expose setToken method', () => {
      expect(store.setToken).to.be.a('function')
    })

    it('setToken method should modify Token state', () => {
      store.setToken('awesometoken')
      expect(store.state.token).to.equal('awesometoken')
      store.setToken(null)
      expect(store.state.token).to.be.null
    })
  })
})
