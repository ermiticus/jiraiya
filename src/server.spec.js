'use strict'

const expect = require('chai').expect
const request = require('supertest')
const httpMocks = require('node-mocks-http')

const server = require('./server')
const characters = require('../src/api/routes/characters')

describe('Server', () => {
  it('Should export a function', () => {
    expect(server).to.be.a('function')
  })

  describe('GET /characters', function() {
    it('respond with json', function(done) {
      request(server)
        .get('/characters')
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(200, done)
    })

    it('respond with json array of letters', done => {
      request(server)
        .get('/characters')
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
          if (err) return done(err)
          let data = JSON.parse(res.text)
          expect(data).to.be.an('array')
          let expected = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","R","S","T","U","V","W","Y","Z","Ō","Ū"]
          expect(data).to.have.same.members(expected)
          return done()
        })
    })

    
  })

  describe('GET /characters/indexon', function() {
    it('respond with json array of names', done => {
      request(server)
        .get('/characters/indexon/A')
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
          if (err) return done(err)
          let data = JSON.parse(res.text)
          expect(data).to.be.an('array')
            .which.length.is.greaterThan(1)
          return done()
        })
    })

    
  })

  after(()=>{
    process.exit(1)
  })
})


