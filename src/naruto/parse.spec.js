'use strict'

const parse = require('./parse')
const expect = require('chai').expect

describe('Parse module', () => {
  describe('"parse"', () => {
    it('Should export a function', () => {
      expect(parse).to.be.a('function')
    })

    it('Sould be a promise', () => {
      expect(parse('http://example.com').then).to.be.a('function')
      expect(parse('http://example.com').catch).to.be.a('function')
    })

    it('Should propagate errors', async () => {
      try {
        const result = await parse('')
      } catch (error) {
        expect(error).to.be.an('Error')
      }
    })
  })
})
