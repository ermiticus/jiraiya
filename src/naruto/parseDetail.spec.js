'use strict'

const parseDetail = require('./parseDetail')
const expect = require('chai').expect

describe('ParseDetail module', () => {
  describe('"parseDetail"', () => {
    it('Should export a function', () => {
      expect(parseDetail).to.be.a('function')
    })

    it('Sould have promise methods', () => {
      try {
        expect(parseDetail('/wiki/Narutopedia').then).to.be.a('function')
        expect(parseDetail('/wiki/Narutopedia').catch).to.be.a('function')
      }
      catch(error) {
        expect(error).to.be.an('Error')
      }
    })

    it('Should propagate errors', async () => {
      try {
        const result = await parseDetail('/')
      } catch (error) {
        expect(error).to.be.an('Error')
      }
    })
  })
})
