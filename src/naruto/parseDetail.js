'use strict'

const axios = require('axios')
const axiosRetry = require('axios-retry')
const cheerio = require('cheerio')

const config = require('../config/config')

function processResponse(response) {
  if (response.status == 200) {
    const html = response.data
    const $ = cheerio.load(html)

    let name = $('.page-header__title').text()
    let imgSrc = $('.imagecell img').first().attr('data-src')
    $('.RelatedForumDiscussion').remove()
    let info = $('#mw-content-text').text()

    return {
      name: name,
      imgSrc: imgSrc,
      info: info
    }
  }
}

function parseDetail(url) {
  url = config.NarutoBaseUrl + url
  
  axiosRetry(axios, { retries: 5 })
  
  return axios
    .get(url, config.axiosOptions)
    .then(processResponse)
    .catch(err => {
      throw err
    })
}

module.exports = parseDetail
