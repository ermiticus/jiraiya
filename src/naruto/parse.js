'use strict'

const axios = require('axios')
const axiosRetry = require('axios-retry')
const cheerio = require('cheerio')

const config = require('../config/config')

function processResponse(response) {
  if (response.status == 200) {
    const html = response.data
    const $ = cheerio.load(html)
    const nextUrl = Array.prototype.map.call(
      $('.category-page__pagination-next'),
      el => el.attribs.href
    )
    let charactersUrls = Array.prototype.map.call(
      $('.category-page__member-link'),
      el => el.attribs.href
    )
    return {
      nextUrl: nextUrl,
      charactersUrls: charactersUrls
    }
  }
}

function parse(url) {
  axiosRetry(axios, { retries: 5 })

  return axios
    .get(url, config.axiosOptions)
    .then(processResponse)
    .catch(err => {
      throw err
    })
}

module.exports = parse
