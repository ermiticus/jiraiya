FROM node:11

# COPY . /app

WORKDIR /app

COPY package*.json ./

RUN yarn install

COPY . .

# build vue js
RUN yarn build

# expose port 80
EXPOSE 80

# launch
CMD ["node","src/server.js"]